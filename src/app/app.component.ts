import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import * as Utils from '../utils';
import * as VApp from '../shapeverse.app';
import * as Verse from '../shapeverse';

const PALETTE:string[] = [
  "#880000", "#AAFFEE", "#CC44CC", "#00CC55", "#0000AA", "#EEEE77",
  "#DD8855", "#664400", "#FF7777", "#333333", "#777777", "#AAFF66", "#0088FF",
  "#BBBBBB"
]

interface IEvent {
  entityId:number;
  type:string;
  data:any;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Shape-Verse';

  // Canvas
  context:CanvasRenderingContext2D;
  canvas:HTMLCanvasElement;
  @ViewChild("myCanvas") myCanvas;
  collisionCanvas:HTMLCanvasElement; // will be used to perform pixel perfect collision
  collisionContext:CanvasRenderingContext2D;
  private displayW:number = 0;
  private displayH:number = 0;

  // Timing
  private m_nDeltaTime:number = 0.0;
  private m_nPrevTime:number = 0.0;
  private m_nFrameCount:number = 0.0;
  private m_nOneSecondTime:number = 0.0;
  private m_nFPS:number = 30;
  private m_nMinDeltaTime:number = 100.0; // 100 ms

  // Scene
  private entitySystem:VApp.EntitySystem;
  private mouseEvents:Array<IEvent>;
  private kbEvents:Array<IEvent>;

  // Interaction
  mouseOverId:number = 0;
  mouseDownId:number = 0 ;
  mouseUpId:number = 0;
  mouseMoveX:number = 0;
  mouseMoveY:number = 0;
  highlightedEntityId:number = 0;
  selectedEntityId:number = 0;
  selectionBox:VApp.Entity = null;

  // Properties info
  appShowProperties:boolean = false;
  appShapeId:number = 0;
  appNoPreviousSave:boolean = false;
  appShowFPS = false;

  ngAfterViewInit() {
    this.canvas = this.myCanvas.nativeElement;
    this.context = this.canvas.getContext("2d");
    // For now, use fixed size: 800x480
    //canvas.width = window.innerWidth - 16;
    //canvas.height = window.innerHeight - 112;
    // TODO: Implement dynamic resizing and full-window size
    this.displayW = this.canvas.width;
    this.displayH = this.canvas.height;
    this.collisionCanvas = document.createElement("canvas");
    this.collisionCanvas.width = this.displayW;
    this.collisionCanvas.height = this.displayH;
    this.collisionContext = this.collisionCanvas.getContext("2d");
    this.m_nPrevTime = Utils.getTime();
    this.entitySystem = new VApp.EntitySystem();
    this.mouseEvents = new Array<IEvent>();
    this.kbEvents = new Array<IEvent>();

    VApp.StaticComponent.Initialize(this.canvas, this.collisionCanvas);

    this.mouseOverId = 0;
    this.mouseDownId = 0;
    this.mouseUpId = 0;
    this.mouseMoveX = 0;
    this.mouseMoveY = 0;
    this.highlightedEntityId = 0;
    this.selectionBox = null;

    this.appShowProperties = false;

    // Create starter shapes
    this.resetScene();

    // Setup events
    this.canvas.addEventListener("mousedown", this.onMouseDown, false);
    this.canvas.addEventListener("mouseup", this.onMouseUp, false);
    this.canvas.addEventListener("mousemove", this.onMouseMove, false);
    window.addEventListener("keydown", this.onKeyDown, false);
    // fix for double-click to select
    this.canvas.onselectstart = function () { return false; } 

    this.tick(0);

    this.checkForPreviousSave();
  }

  onMouseDown = (event:MouseEvent):void => {
    let mx = event.x - this.canvas.offsetLeft;
    let my = event.y - this.canvas.offsetTop + window.pageYOffset;
    let blocked_by_ui = false;
    // Selection-box can block entity interaction with mouse
    if(VApp.StaticComponent.selectionBoxUI) {
      if(VApp.StaticComponent.selectionBoxUI.isPointInBox(mx, my) || 
         VApp.StaticComponent.selectionBoxUI.isPointInRotation(mx, my)) {
        VApp.StaticComponent.selectionBoxUI.mouseDown(mx,  my);
        blocked_by_ui = true;
      }
    }
    if(!blocked_by_ui) {
      let cc = this.collisionContext.getImageData(mx, my, 1, 1);
      let eid = VApp.RGBToEntityId(cc.data);
      if(eid > 0 || this.selectedEntityId > 0) {
        this.mouseEvents.push({
          entityId: eid, 
          type: "mousedown", 
          data: [mx, my]
        });
      }
    }
  }

  onMouseUp = (event:MouseEvent):void => {
    let mx = event.x - this.canvas.offsetLeft;
    let my = event.y - this.canvas.offsetTop + window.pageYOffset;
    let blocked_by_ui = false;
    // Selection-box can block entity interaction with mouse
    if(VApp.StaticComponent.selectionBoxUI) {
      if(VApp.StaticComponent.selectionBoxUI.isBeingDragged) {
        // If being dragged just send mouse up event and block UI
        VApp.StaticComponent.selectionBoxUI.mouseUp(this.mouseMoveX,  this.mouseMoveY);
        blocked_by_ui = true;
      } else if(VApp.StaticComponent.selectionBoxUI.isPointInBox(mx, my) || 
                VApp.StaticComponent.selectionBoxUI.isPointInRotation(mx, my)) {
        VApp.StaticComponent.selectionBoxUI.mouseUp(mx, my);
        blocked_by_ui = true;
      }
    }
    if(!blocked_by_ui) {
      let cc = this.collisionContext.getImageData(mx, my, 1, 1);
      let eid = VApp.RGBToEntityId(cc.data);
      if(eid > 0) {
        this.mouseEvents.push({
          entityId: eid, 
          type: "mouseup", 
          data: [mx, my]
        });
      }
    }
  }

  onMouseMove = (event:MouseEvent):void => {
    let mx = this.mouseMoveX = event.x - this.canvas.offsetLeft;
    let my = this.mouseMoveY = event.y - this.canvas.offsetTop + window.pageYOffset;
    let blocked_by_ui = false;
    // Selection-box can block shape interaction with mouse
    // TODO: do a collision check with entity to unblock if it's clicking through the shape?
    if(VApp.StaticComponent.selectionBoxUI) {
      if(VApp.StaticComponent.selectionBoxUI.isBeingDragged) {
        // If being dragged just send mouse move event and block UI
        VApp.StaticComponent.selectionBoxUI.mouseMove(mx,  my);
        blocked_by_ui = true;
      } else if(VApp.StaticComponent.selectionBoxUI.isPointInBox(mx, my) || 
                VApp.StaticComponent.selectionBoxUI.isPointInRotation(mx, my)) {
        VApp.StaticComponent.selectionBoxUI.mouseOver = true;
        VApp.StaticComponent.selectionBoxUI.mouseMove(mx,  my);
        blocked_by_ui = true;
      } else {
        VApp.StaticComponent.selectionBoxUI.mouseOver = false;
      }
    }
    if(!blocked_by_ui) {
      let cc = this.collisionContext.getImageData(mx, my, 1, 1);
      let eid = VApp.RGBToEntityId(cc.data);
      // Mouse move is dispatched if there is an entity now or there was previous frame
      if(eid > 0 || this.highlightedEntityId > 0) {
        this.mouseEvents.push({
          entityId: eid, 
          type: "mousemove", 
          data: [mx, my]
        });
      }
    }
  }

  onKeyDown = (event:KeyboardEvent):void => {
    this.kbEvents.push({
      entityId: 0,
      type: "keydown",
      data: [event.keyCode, event.key]
    });
  }

  tick(timestamp) {
    requestAnimationFrame((ts) => {
      this.tick(ts);
    });

    this.tickTiming(timestamp);

    var ctx = this.context;

    // Background
    ctx.clearRect(0, 0, this.displayW, this.displayH);
    this.collisionContext.clearRect(0, 0, this.displayW, this.displayH);

    // Entities: Update all 
    this.entitySystem.update(this.m_nDeltaTime);

    // Mouse events dispatch
    this.handleMouseEvents();
    // Keyboard events dispatch
    this.handleKeyboardEvents();

    // Entities: Render all using sorted list
    this.entitySystem.draw(ctx, this.collisionContext);

    if(this.appShowFPS) {
      this.showFPS();
    }

  }

  handleMouseEvents():void {
    while(this.mouseEvents.length > 0) {
      let e = this.mouseEvents.pop();
      let ent = this.entitySystem.get(e.entityId);

      // Mouse move needs to know when mouse is no longer over an entity
      // to call: mouseExit
      if(e.type == "mousemove") {
        if(!ent) {
          // No entity there
          if(this.highlightedEntityId > 0) {
            this.entitySystem.get(this.highlightedEntityId).mouseExit();
            this.highlightedEntityId = 0;
          }
        } else { // mouse moving on an entity
          if(ent.id != this.highlightedEntityId) {
            if(this.highlightedEntityId > 0) {
              this.entitySystem.get(this.highlightedEntityId).mouseExit();
            }
            if(ent.id > 0) {
              ent.mouseEnter();
            }
          }
          this.highlightedEntityId = ent.id;
          let consumed = ent.mouseMove(e.data[0], e.data[1]);
        }
      }

      if(e.type == "mousedown") {
        if(ent) {
          let consumed = ent.mouseDown(e.data[0], e.data[1]);
          if(consumed) {
            if(this.selectedEntityId > 0) {
              this.unselectEntity()
            } 
            this.selectEntity(ent);
            // Allow direct drag-drop immediately after selection
            if(VApp.StaticComponent.selectionBoxUI) {
              VApp.StaticComponent.selectionBoxUI.mouseDown(e.data[0],  e.data[1]);
            }
          }
        } else if(e.entityId == 0) {
          // handle clicking on nothing
          if(this.selectedEntityId > 0) {
            this.unselectEntity();
          }
        }
      }

      if(e.type == "mouseup") {
        if(ent) {
          let consumed = ent.mouseUp(e.data[0], e.data[1]);
        }
      }
    }
  }

  handleKeyboardEvents():void {
    while(this.kbEvents.length > 0) {
      let e = this.kbEvents.pop();
      if(e.type == "keydown") {
        // Backspace or Delete
        if(e.data[0] == 8 || e.data[0] == 46) {
          if(this.selectedEntityId > 0) {
            let ent = this.entitySystem.get(this.selectedEntityId);
            this.unselectEntity();
            this.removeEntity(ent.id);
          }
        } else if(e.data[1] == '~' || e.data[1] == '§') {
          this.appShowFPS = !this.appShowFPS;
        }
      }
    }
  }

  populateProperties(selected_entity:VApp.Entity):void {
    this.appShowProperties = true;
    this.appShapeId = selected_entity.id;
  }

  hideProperties():void {
    this.appShowProperties = false;
  }

  selectEntity(entity:VApp.Entity):void {
    if(this.selectedEntityId > 0) {
      this.unselectEntity();
    }
    entity.select();
    // Create transform box
    this.selectionBox = new VApp.TransformBox(entity);
    this.addEntity(this.selectionBox);
    this.selectedEntityId = entity.id;
    this.populateProperties(this.entitySystem.get(this.selectedEntityId));
  }

  unselectEntity():void {
    if(this.selectedEntityId == 0) { // make it safe to call anytime
      return;
    }
    let entity = this.entitySystem.get(this.selectedEntityId);
    if(this.highlightedEntityId == this.selectedEntityId) {
      this.highlightedEntityId = 0;
    }
    entity.unselect();
    // Delete transform box
    this.removeEntity(this.selectionBox.id);
    this.selectionBox = null;
    this.selectedEntityId = 0;
    this.hideProperties();
  }

  addEntity(entity:VApp.Entity, keep_selection:boolean = false) {
    if(!keep_selection && this.selectedEntityId > 0) {
      this.unselectEntity();
    }
    this.entitySystem.addEntity(entity);
  }

  removeEntity(entity_id:number) {
    if(this.highlightedEntityId == entity_id) {
      this.highlightedEntityId = 0;
    }
    if(this.selectedEntityId == entity_id) {
      this.selectedEntityId = 0;
    }
    this.entitySystem.removeEntity(this.entitySystem.get(entity_id));
  }

  removeAllEntities() {
    this.entitySystem.removeAllEntities();
  }

  tickTiming(timestamp:number):void {
    // FPS and timing
    let curr_time = timestamp;
    // Delta time limited by nMinDeltaTime to mitigate large timestep
    this.m_nDeltaTime = Math.min(Math.abs((curr_time - this.m_nPrevTime) / 1000.0), 
      this.m_nMinDeltaTime / 1000.0);
    this.m_nPrevTime = curr_time;
    this.m_nFrameCount++;
    this.m_nOneSecondTime += this.m_nDeltaTime;
    while(this.m_nOneSecondTime >= 1.0) {
      this.m_nOneSecondTime -= 1.0;
      this.m_nFPS = this.m_nFrameCount;
      this.m_nFrameCount = 0.0;
    }
  }

  showFPS():void {
    let fps_fontsize = 12;
    let fps_x:number = 1;
    let fps_y:number = fps_fontsize;
    this.context.font = fps_fontsize.toString() + "px Arial";
    let fps_text = "FPS: " + this.m_nFPS.toString();
    this.context.fillStyle = "#000000";
    this.context.fillText(fps_text, fps_x + 1, fps_y + 1);
    this.context.fillStyle = "#ffffff";
    this.context.fillText(fps_text, fps_x, fps_y);
  }

  checkForPreviousSave():boolean {
    this.appNoPreviousSave = window.localStorage.getItem("VerseScene") == null;
    return this.appNoPreviousSave;
  }

  // Used for button [disabled] method
  isNoPreviousSaveFound():boolean {
    return this.appNoPreviousSave;
  }

  serializeEntities():string {
    let data = [];
    for(let e of this.entitySystem.entitiesArray) {
      if(e.isSaveData) {
        data.push(e.serialize());
      };
    }
    console.log("SAVE DATA = ");
    console.log(JSON.stringify(data));
    return JSON.stringify(data);
  }

  unserializeEntities(save_data:string):void {
    let edata = JSON.parse(save_data);
    let entity = null;
    if(edata) {
      // Loop over saved entities
      for(let e of edata) {
        // Create new entities using unserializer
        if(e.type == "Entity") {
          entity = VApp.Entity.unserialize(e);
        } else if(e.type == "Shape") {
          entity = VApp.Shape.unserialize(e);
        } else {
          console.log("Unknown top level type in save data: " + e.type);
        }
        if(entity) {
          console.log("Unserialized entity #" + e.entityId + " of type " + e.type);
          this.addEntity(entity);
        }
      }
      this.entitySystem.resortEntities();
    }
  }

  get selectedShapeColor():string {
    let color = "#ff00ff";
    if(this.selectedEntityId) {
      let ent:VApp.Entity = this.entitySystem.get(this.selectedEntityId);
      color = ent.drawable.getFillColor();
    }
    return color;
  }

  set selectedShapeColor(color:string) {
    if(this.selectedEntityId) {
      let ent:VApp.Entity = this.entitySystem.get(this.selectedEntityId);
      ent.drawable.setFillColor(color);
    }
  }

  get selectedShapeLayer():number {
    if(this.selectedEntityId) {
      let ent:VApp.Entity = this.entitySystem.get(this.selectedEntityId);
      return ent.layer;
    }
    return 0;
  }

  //------------------------------------------ Angular 2 actions
  resetScene():void {
    // Remove all entities
    this.unselectEntity();
    this.removeAllEntities();

    // Create starter shapes
    {
      let stepx = this.canvas.width / 5;
      
      let square = new VApp.Shape(new Verse.Material(true, true, "#CC44CC", "#000000", 1.0));
      square.addRectComponent(stepx * 1, 200, 150, 150);
      
      let circle = new VApp.Shape(new Verse.Material(true, true, "#00CC55"));
      circle.addCircleComponent(stepx * 2, 200, 75 );
      
      let triangle = new VApp.Shape(new Verse.Material(true, true, "#0000AA"));
      triangle.addTriangleComponent(stepx * 3, 200, 150, 150);

      let star = new VApp.Shape(new Verse.Material(true, true, "#EEEE77"));
      star.addStarComponent(stepx * 4, 200, 75);

      // Add all to entities
      this.addEntity(square);
      this.addEntity(circle);
      this.addEntity(triangle);
      this.addEntity(star);
    }
  }
  loadScene():void {
    let it = window.localStorage.getItem("VerseScene");
    if(it == "true") {
      console.log("Loading previous saved scene...")
      this.unselectEntity();
      this.removeAllEntities();
      this.unserializeEntities(window.localStorage.getItem("savedata"));
      console.log("done.");
    } else {
      alert("No previously saved scene found");
    }
  }
  saveScene():void {
    let it = window.localStorage.getItem("VerseScene");
    if(it) {
      // Clear old save
      window.localStorage.clear();
    }
    window.localStorage.setItem("VerseScene", "true");
    window.localStorage.setItem("savedata", this.serializeEntities());
    this.checkForPreviousSave();
    console.log("done.");
  }

  private getRandomShapeProperties():any {
    let out = {}
    out["x"] = Math.random() * (this.canvas.width - 200) + 100;
    out["y"] = Math.random() * (this.canvas.height - 200) + 100; 
    out["c"] = PALETTE[Math.floor(Math.random() * PALETTE.length)];
    out["sw"] = Math.random() * 200 + 50;
    out["sh"] = Math.random() * 200 + 5;
    return out;
  }
  addSquare():void {
    let r = this.getRandomShapeProperties();
    let square = new VApp.Shape(new Verse.Material(true, true, r.c, "#000000", 1.0));
    square.addRectComponent(r.x, r.y, r.sw, r.sh);      
    this.addEntity(square)
  }

  addTriangle():void {
    let r = this.getRandomShapeProperties();
    let triangle = new VApp.Shape(new Verse.Material(true, true, r.c));
    triangle.addTriangleComponent(r.x, r.y, r.sw, r.sh);
    this.addEntity(triangle);
  }

  addCircle():void {
    let r = this.getRandomShapeProperties();
    let circle = new VApp.Shape(new Verse.Material(true, true, r.c));
    circle.addCircleComponent(r.x, r.y, r.sw / 2 );
    this.addEntity(circle);
  }

  addStar():void {
    let r = this.getRandomShapeProperties();
    let star = new VApp.Shape(new Verse.Material(true, true, r.c));
    star.addStarComponent(r.x, r.y, r.sw / 2);
    this.addEntity(star);
  }

  deleteSelectedShape():void {
    if(this.selectedEntityId > 0) {
      let eid = this.selectedEntityId;
      this.unselectEntity();
      this.removeEntity(eid);
    }
  }

  cloneSelectedShape():void {
    if(this.selectedEntityId > 0) {
      let eid = this.selectedEntityId;
      let data = this.entitySystem.get(eid).serialize();
      let clone:VApp.Entity = VApp.Shape.unserialize(data);
      let r = this.getRandomShapeProperties();
      clone.transform.x = r.x;
      clone.transform.y = r.y;
      this.addEntity(clone, true);
      this.selectEntity(clone);
    }
  }

  mirrorHSelectedShape():void {
    if(this.selectedEntityId > 0) {
      let eid = this.selectedEntityId;
      this.entitySystem.get(eid).mirrorH();
    }
  }

  mirrorVSelectedShape():void {
    if(this.selectedEntityId > 0) {
      let eid = this.selectedEntityId;
      this.entitySystem.get(eid).mirrorV();
    }
  }

  incrementSelectedLayer():void {
    if(this.selectedEntityId) {
      let ent:VApp.Entity = this.entitySystem.get(this.selectedEntityId);
      ent.layer += 1;
      this.entitySystem.resortEntities();
    }
  }

  decrementSelectedLayer():void {
    if(this.selectedEntityId) {
      let ent:VApp.Entity = this.entitySystem.get(this.selectedEntityId);
      ent.layer -= 1;
      this.entitySystem.resortEntities();
    }
  }
}