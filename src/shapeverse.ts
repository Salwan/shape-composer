import * as assert from 'assert';
import { vec2 } from 'shapemath';

// Of shapes and their nature

// Component-based architecture
// Shape-Verse uses a simplified ECS-system implementation.

// --------------------- Data components
// Material is used to specify how shapes are rendered
export class Material {
    drawFill:boolean;
    drawStroke:boolean;
    fillColor:string;
    strokeColor:string;
    strokeThickness:number;

    constructor(has_fill:boolean = true, has_stroke:boolean = false, fill_color:string = "#808080", stroke_color:string = "#000000",stroke_thickness:number = 1.0) {
        this.drawFill = has_fill;
        this.drawStroke = has_stroke;
        this.fillColor = fill_color;
        this.strokeColor = stroke_color;
        this.strokeThickness = stroke_thickness;
    }

    clone(mtrl:Material):void {
        if(mtrl) {
            this.drawFill = mtrl.drawFill;
            this.drawStroke = mtrl.drawStroke;
            this.fillColor = mtrl.fillColor;
            this.strokeColor = mtrl.strokeColor;
            this.strokeThickness = mtrl.strokeThickness;
        }
    }
}
// Transform is a default component instanced for every object
export class Transform {
    x:number;
    y:number;
    rotation:number;
    scalex:number;
    scaley:number;

    public constructor(_x:number = 0, _y:number = 0) {
        this.x = _x;
        this.y = _y;
        this.rotation = 0.0;
        this.scalex = 1.0;
        this.scaley = 1.0;
    }

    public static unserialize(data:any):Transform {
        let t = new Transform();
        t.x = data.x;
        t.y = data.y;
        t.rotation = data.rotation;
        t.scalex = data.scalex;
        t.scaley = data.scaley;
        return t;
    }

    clone(t:Transform):void {
        this.x = t.x;
        this.y = t.y;
        this.rotation = t.rotation;
        this.scalex = t.scalex;
        this.scaley = t.scaley;
    }

    public isEqual(transform:Transform):boolean {
        if(this.x != transform.x || this.y != transform.y) {
            return false;
        }
        if(this.rotation != transform.rotation) {
            return false;
        }
        if(this.scalex != transform.scalex || this.scaley != transform.scaley) {
            return false;
        }
        return true;
    }
}
// AABB is for simple bounding boxes
export class AABB {
    minX:number;
    maxX:number;
    minY:number;
    maxY:number;

    public constructor(_minX:number = 0, _maxX:number = 0, _minY:number = 0, _maxY:number = 0) {
        this.minX = _minX;
        this.maxX = _maxX;
        this.minY = _minY;
        this.maxY = _maxY;
    }
    public isPointInside(px:number, py:number):boolean {
        if( px < this.minX || px > this.maxX || 
            py < this.minY || py > this.maxY) {
            return false;
        } else {
            return true;
        }
    }

    get width():number {
        return Math.abs(this.maxX - this.minX);
    }

    get height():number {
        return Math.abs(this.maxY - this.minY);
    }

    get center():any {
        return [this.minX + this.width / 2, this.minY + this.height / 2];
    }
}
// Vertices represents a Vertex-based shape where vertices are points.
// - Generates Path2D for use with canvas
// - Can apply transformation to all vertices
// - Allow modifying and updating vertices anytime
export class Vertices {
    // Serializable
    protected verts:Array<vec2>;

    // Non-serializable
    protected dirty:boolean; // used to determine when to regen Path2D
    protected m_path:Path2D;
    protected m_transform:Transform;
    protected m_lastTransform:Transform; // used to detect transform changes
    protected m_aabb:AABB;

    public constructor(_vertices?:Array<vec2>, _init_transform?:Transform) {
        this.verts = new Array<vec2>();
        this.dirty = false;
        this.m_transform = new Transform();
        if(_init_transform) {
            this.m_transform.clone(_init_transform);
        }
        this.m_lastTransform = new Transform();
        this.m_lastTransform.clone(this.m_transform);
        if(_vertices != null && _vertices.length > 0) {
            this.verts.concat(_vertices);
            this.dirty = true;
        }
    }

    clone(verts:Vertices):void {
        this.verts = new Array<vec2>();
        for(let v of verts.verts) {
            this.verts.push(new vec2(v.x, v.y));
        }
        this.dirty = verts.dirty;
        this.m_path = verts.m_path;
        this.m_transform = verts.m_transform;
        this.m_lastTransform = verts.m_lastTransform;
        this.m_aabb = verts.m_aabb;
    }

    add(vx:number, vy:number):void {
        this.verts.push(new vec2(vx, vy));
        this.dirty = true;
    }

    concat(_vertices:Vertices):void {
        this.verts = this.verts.concat(_vertices.verts);
        this.dirty = true;
    }

    protected calculateAABB():AABB {
        let aabb:AABB = new AABB();
        aabb.minX = aabb.minY = Number.MAX_VALUE;
        aabb.maxX = aabb.maxY = Number.MIN_VALUE;
        for(let v of this.verts) {
            aabb.minX = v.x < aabb.minX? v.x : aabb.minX;
            aabb.minY = v.y < aabb.minY? v.y : aabb.minY;
            aabb.maxX = v.x > aabb.maxX? v.x : aabb.maxX;
            aabb.maxY = v.y > aabb.maxY? v.y : aabb.maxY;
        }
        return aabb;
    }

    protected updateVertices():void {
        let aabb = this.aabb;
        if(this.m_transform == null)
        console.log("WHAT THE FACK" + this.m_transform);
        if(!this.m_transform.isEqual(this.m_lastTransform)) {
            // Scaling difference
            let scx = this.m_transform.scalex - this.m_lastTransform.scalex;
            let scy = this.m_transform.scaley - this.m_lastTransform.scaley;
            // Rotation difference
            let rot = this.m_transform.rotation - this.m_lastTransform.rotation;
            //
            for(let v of this.verts) {
                // Transform back to origin
                v.x -= this.m_lastTransform.x;
                v.y -= this.m_lastTransform.y;
                if(scx + scy != 0.0) {
                    // Scale back to 1.0
                    v.scalexy(1.0 / this.m_lastTransform.scalex,
                        1.0 / this.m_lastTransform.scaley); 
                    v.scalexy(this.m_transform.scalex, 
                        this.m_transform.scaley);
                }
                if(rot != 0.0) {
                    //v.rotate(this.m_transform.rotation, 
                    //         new vec2(this.m_transform.x,this.m_transform.y));
                    v.rotate(this.m_transform.rotation);
                }
                // Translation
                v.x += this.m_transform.x;
                v.y += this.m_transform.y;
            }
        }
        this.m_lastTransform.clone(this.m_transform);
        // Rebuild path
        this.m_path = new Path2D();
        for(let i = 0; i < this.verts.length; ++i) {
            this.m_path.lineTo(this.verts[i].x, this.verts[i].y);
        }
        this.m_path.lineTo(this.verts[0].x, this.verts[0].y);
        // Recalculate AABB
        this.m_aabb = this.calculateAABB();
    }

    get length():number {
        return this.verts.length;
    }

    get path():Path2D {
        assert(this.length > 2, "Vertices object has less than 3 vertices: " + this.length);
        if(this.dirty) {
            this.updateVertices();
            this.dirty = false;
        }
        return this.m_path;
    }

    get aabb():AABB {
        if(this.dirty) {
            this.dirty = false;
            this.updateVertices();
        }
        return this.m_aabb;
    }

    get transform():Transform {
        return this.m_transform;
    }

    set transform(t:Transform) {
        this.m_transform = t;
        this.dirty = true;
    }

    get array():Array<vec2> {
        return this.verts;
    }
}

// --------------------- General entity interface
export interface IEntity {
    transform:Transform;
    addComponent(comp:IComponent):void;
    update(time_delta:number):void;
    draw(context:CanvasRenderingContext2D):void;
    drawInteractables(context:CanvasRenderingContext2D, collision_context:CanvasRenderingContext2D):void;
    notifyRemoval():void;
    serialize():any;
}

// --------------------- Component interfaces
export interface IComponent {  
    serialize():any;
}

// Defines a drawable component, draw called multiple times per-frame 
// to draw different things (collision mask, highlighting, ...)
export interface IDrawable extends IComponent {
    draw(context:CanvasRenderingContext2D, material_override:Material):void;
    // the bounding box of the drawable
    getAABB():AABB;
    getFillColor():string;
    setFillColor(color:string):void;
}

// Defines an updateable interface, update gets called on every tick.
export interface IUpdatable extends IComponent {
    update(time_delta:number):void;
}

// Defines any interactable objects:
// - Draw interaction shape to collision context (for pixel-perfect mouse collision)
// - Can be selected/unselected (visual representation) 
// - Can be highlighted on mouse move (visual representation)
export interface IInteractable extends IComponent {
    isSelected:boolean;
    isHighlighted:boolean;
    collisionMaterial:Material;
    drawInteraction(context:CanvasRenderingContext2D, collision_context:CanvasRenderingContext2D):void
    mouseDown(mx:number, my:number):boolean;
    mouseUp(mx:number, my:number):boolean;
    mouseMove(mx:number, my:number):boolean;
    mouseExit():boolean;
    mouseEnter():boolean;
    select():void;
    unselect():void;
}

export interface IUIComponent extends IComponent {
    boundingBox:AABB;
    mouseDown(mx:number, my:number):boolean;
    mouseUp(mx:number, my:number):boolean;
    mouseMove(mx:number, my:number):boolean;
    mouseExit():boolean;
    mouseEnter():boolean;
}

// Component type guards
export function isDrawable(obj:IComponent | IDrawable | IUpdatable | IInteractable | IUIComponent):obj is IDrawable {
    return (<IDrawable>obj).draw !== undefined;
}
export function isUpdatable(obj:IComponent | IDrawable | IUpdatable | IInteractable | IUIComponent):obj is IUpdatable {
    return (<IUpdatable>obj).update !== undefined;
}
export function isInteractable(obj:IComponent | IDrawable | IUpdatable | IInteractable | IUIComponent):obj is IInteractable {
    return (<IInteractable>obj).drawInteraction !== undefined;
}
export function isUIComponent(obj:IComponent | IDrawable | IUpdatable | IInteractable | IUIComponent):obj is IUIComponent {
    return (<IUIComponent>obj).boundingBox !== undefined;
}

