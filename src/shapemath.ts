// Math of shapeverse

// Vector 2D
export class vec2 {
    x:number;
    y:number;

    public constructor(_x:number = 0.0, _y:number = 0.0) {
        this.x = _x;
        this.y = _y;
    }

    public set(_x:number, _y:number) {
        this.x = _x;
        this.y = _y;
    }

    // Applies: adds vector b to this vector
    public add(b:vec2):void {
        this.x += b.x;
        this.y += b.y;
    }

    // Applies: subtract vector b from this vector
    public sub(b:vec2):void {
        this.x -= b.x;
        this.y -= b.y;
    }

    // Applies: component multiplication
    public mul(b:vec2):void {
        this.x *= b.x;
        this.y *= b.y;
    }

    // Applies: Multiply x and y with given number.
    public scale(n:number):void {
        this.x *= n;
        this.y *= n;
    }

    // Applies: component division
    public div(b:vec2):void {
        this.x /= b.x;
        this.y /= b.y;
    }

    // Calculates dot product of vector b and this one
    // (does not change this vector)
    public dot(b:vec2):number {
        return (this.x * b.x) + (this.y * b.y);
    }

    // Calculates direction of this vector in radians
    // (does not change this vector)
    public direction():number {
        if(this.x != 0.0) {
            return Math.atan2(this.y, this.x);
        } else if(this.y != 0.0) {
                return this.y > 0.0? 90.0 * Math.PI / 180.0 : -90.0 * Math.PI / 180.0;    
        } else {
            // considered domain error
            // returning zero following standard IEC-60559/IEEE 754
            return 0.0; 
        }
    }

    // Calculates magnitude of vec2
    // (does not change this vector)
    public distance():number {
        return Math.sqrt((this.x * this.x) + (this.y * this.y)); 
    }

    public distanceSq():number {
        return (this.x * this.x) + (this.y * this.y);
    }

    // Applies: Normalization to this vector
    public normalize():void {
        let m:number = this.distance();
        this.x /= m;
        this.y /= m;
    }

    // Calculates and returns the normal for this vec2
    // (does not change this vector)
    public norm():vec2 {
        let m:number = this.distance();
        return new vec2(this.x / m, this.y / m);
    }

    // Rotates vector around a given point (or origin if null).
    public rotate(radians:number, around_point:vec2 = null):void {
        let p:vec2 = around_point != null? around_point : new vec2(0, 0);
        let x2 = p.x + ((this.x - p.x) * Math.cos(radians)) - 
                 ((this.y - p.y) * Math.sin(radians));
        let y2 = p.y + ((this.x - p.x) * Math.sin(radians)) +
                 ((this.y - p.y) * Math.cos(radians));
        this.x = x2;
        this.y = y2;
    }

    public scalexy(sx:number, sy:number):void {
        let x2 = this.x * sx;
        let y2 = this.y * sy;
        this.x = x2;
        this.y = y2;
    }

}

// Vector 2D standaone math functions for convenience

// Adds two vectors
export function v2add(a:vec2, b:vec2):vec2 {
    return new vec2(a.x + b.x, a.y + b.y);
}

// Subtract vector b from vector a
export function v2sub(a:vec2, b:vec2):vec2 {
    return new vec2(a.x - b.x, a.y - b.y);
}

// Component multiplies 2 vectors
export function v2mul(a:vec2, b:vec2):vec2 {
    return new vec2(a.x * b.x, a.y * b.y);
}

// Scale vector by N
export function v2scale(a:vec2, n:number):vec2 {
    return new vec2(a.x * n, a.y * n);
}

// Component divides 2 vectors
export function v2div(a:vec2, b:vec2):vec2 {
    return new vec2(a.x / b.x, a.y / b.y);
}

// Calculates dot product between two vectors
export function v2dot(a:vec2, b:vec2):number {
    return (a.x * b.x) + (a.y * b.y);
}

// Calculates distance between two vectors
export function v2distance(a:vec2, b:vec2):number {
    return v2sub(b, a).distance();
}

// Calculates square of distance between two vectors
export function v2distance2(a:vec2, b:vec2):number {
    return v2sub(b, a).distanceSq();
}


