import { TestBed, async } from '@angular/core/testing';
import * as ShapeMath from 'shapemath'

describe('ShapeMath', () => {
    var v1, v2, v3, vo;
    var sx, sy;
    var dist;
    var angle;
    beforeEach(async(() => {
        v1 = new ShapeMath.vec2(10, 10);
        v2 = new ShapeMath.vec2(5, 5);
        v3 = new ShapeMath.vec2(-1, 0);
        vo = new ShapeMath.vec2();
        sx = 1.33;
        sy = 1.33;
        angle = 50 * Math.PI / 180.0;
    }));
    it('should measure distance of vector', async(() => {
        dist = v1.distance();
        expect(dist).toBeCloseTo(14.1421, 0.01);
    }));
    it('should scale a vector by x and y', async(() => {
        v1.scalexy(sx, sy);
        expect(v1.x).toBeCloseTo(10 * sx, 0.01);
        expect(v1.y).toBeCloseTo(10 * sy, 0.01);
    }));
    it('should normalize a vector correctly', async(() => {
        v2.normalize();
        expect(v2.distance()).toBeCloseTo(1.0, 0.01);
    }));
    it('should calculate direction', async(() => {
        expect(v3.direction()).toBeCloseTo(180.0 * Math.PI / 180, 0.01);
    }));
    it('should calculate dot-product', async(() => {
        expect(v1.dot(v2)).toBe(100);
    }));
    it('should add two vectors', async(() => {
        v1.add(v2);
        expect(v1.distance()).toBeCloseTo(Math.sqrt((15 * 15) + (15 * 15)), 0.01);
    }));
    it('should rotate vector around a point', async(() => {
        v3.rotate(-90 * Math.PI / 180.0, new ShapeMath.vec2(0, 0));
        expect(v3.direction()).toBeCloseTo(90.0 * Math.PI / 180.0, 0.01);
    }))
});
