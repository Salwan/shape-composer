// Useful general utilties

////////////////////////////////////////////////////// Utils
export function clamp(num, min, max):number {
    return num <= min ? min : num >= max ? max : num;
}

export function getTime():number {
    return +Date.now();
}