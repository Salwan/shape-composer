import { Material, Transform, AABB, Vertices, IEntity, IComponent, IDrawable, IUpdatable, IInteractable, IUIComponent, isDrawable, isUpdatable, isInteractable, isUIComponent } from 'shapeverse';
import * as assert from 'assert';
import { vec2, v2add, v2sub, v2mul, v2div, v2scale, v2dot, v2distance2 } from 'shapemath';

// A shape is defined by the following parameters:
// - stroke color
// - thickness of stroke
// - fill color
// - a drawable geometry component: vertices or circle
// - a transformation component
// - a basic collider shape (for mouse interactions)

// --------------------- Utility functions
const   ENTITY_COLOR_MULTIPLIER :number = 0x20;
const   CIRCLE_SEGMENTS         :number = 32.0;
const   SHAPES_LAYER            :number = 100;
const   TRANSFORM_BOX_LAYER     :number = 1000;
const   TRANSFORM_BOX_SCALE_BORDER : number = 7;


// Converts entity id to html color as a string: #NNNNNN
function EntityIdToColor(entity_id:number):string {
    let color = (entity_id * ENTITY_COLOR_MULTIPLIER).toString(16);
    assert(color.length <= 6);
    while(color.length < 6) {
        color = "0" + color;
    }
    return "#" + color;
}
// Converts html color (#NNNNNN) to entity id
function ColorToEntityId(color:string):number {
    return (parseInt(color.substr(1), 16) & 0xffffff) / ENTITY_COLOR_MULTIPLIER;
}
// Data given as: [R,G,B,A]
export function RGBToEntityId(data:Uint8ClampedArray):number {
    return ((data[0] << 16) + (data[1] << 8) + (data[2])) / ENTITY_COLOR_MULTIPLIER;
}

// --------------------- Component classes

// CDrawable draws any Path, it has no awareness of bounding box.
abstract class CDrawable implements IDrawable {
    static readonly ClassName:string = "CDrawable";

    // Serializables
    protected material:Material;

    // Non-serializables
    protected entity:Entity;

    public constructor(_entity:Entity, _material:Material) {
        this.entity = _entity;
        this.material = new Material();
        if(_material) {
            this.material.clone(_material);
        }
    }

    public static unserialize(data:any):CDrawable {
        assert(false, "CDrawable is unserializable directly as it's an abstract class");
        return null;
    }

    serialize():any {
        let data = {};
        data["type"] = CDrawable.ClassName; // overrides parent type name
        data["material"] = this.material;
        return data;
    }

    abstract draw(context:CanvasRenderingContext2D, material_override:Material):void;

    // Actualyl draws the given path or just directly calls fill
    // Sets up correct stroke and fill material settings.
    protected executeCanvasDraw(context:CanvasRenderingContext2D, _path:Path2D = null, material_override:Material = null) {
        let rmaterial = material_override == null? this.material : material_override;
        if(rmaterial.drawFill) {
            context.fillStyle = rmaterial.fillColor;
            if(_path) {
                context.fill(_path);
            } else {
                context.fill();
            }
        }
        if(rmaterial.drawStroke) {
            context.strokeStyle = rmaterial.strokeColor;
            context.lineWidth = rmaterial.strokeThickness;
            if(_path) {
                context.stroke(_path);
            } else {
                context.stroke();
            }
        }
    }

    getAABB():AABB {
        // No AABB for CDrawable
        assert(true);
        return new AABB();
    }

    getFillColor():string {
        return this.material.fillColor;
    }

    setFillColor(color:string):void {
        this.material.fillColor = color.toString();
    }
}

// Vertex drawable used for transformable/interactable things
class CDrawableVertices extends CDrawable {
    static readonly ClassName:string = "CDrawableVertices";

    // Serializables
    protected vertices:Vertices;

    public constructor(_entity:Entity, _material:Material, _vertices:Vertices = null) {
        super(_entity, _material);
        this.vertices = new Vertices();
        if(_vertices && _vertices.length > 0) {
            this.vertices.clone(_vertices);
        }
    }
    draw(context:CanvasRenderingContext2D = null, material_override:Material = null):void {
        this.vertices.transform = this.entity.transform;
        context.save();
        this.executeCanvasDraw(context, this.vertices.path, material_override);
        context.restore();
    }

    getAABB():AABB {
        return this.vertices.aabb;
    }

    serialize():any {
        let data = super.serialize();
        data.type = CDrawableVertices.ClassName;
        data.vertices = this.vertices.array;
        data.verticesTransform = this.vertices.transform;
        return data;
    }
}

export class CInteractable implements IInteractable {
    static readonly ClassName:string = "CInteractable";

    // Serializables

    // Non-serializables
    collisionMaterial:Material;
    highlightMaterial:Material;
    selectedMaterial:Material;
    isSelected:boolean;
    isHighlighted:boolean;
    drawable:IDrawable;

    public constructor(entity_id:number, drawable_component:IDrawable) {
        assert(drawable_component);
        this.isSelected = false;
        this.isHighlighted = false;
        this.drawable = drawable_component;
        this.collisionMaterial = new Material(true, false, 
            EntityIdToColor(entity_id));
        this.highlightMaterial = new Material(false, true, "#000000", 
            "#ffffff", 3.0);
        this.selectedMaterial = new Material(false, true, "#000000", 
            "#00ff20", 3.0); 
    }
    drawInteraction(context:CanvasRenderingContext2D, collision_context:CanvasRenderingContext2D):void {
        // Draw collision mask
        this.drawable.draw(collision_context, this.collisionMaterial);
        // Draw interaction
        if(this.isSelected) {
            this.drawable.draw(context, this.selectedMaterial);
        } else if(this.isHighlighted) {
            this.drawable.draw(context, this.highlightMaterial);
        }
    }
    mouseEnter():boolean {
        this.isHighlighted = true;
        return true;
    }
    mouseExit():boolean {
        this.isHighlighted = false;
        return true;
    }
    mouseUp(mx:number, my:number):boolean {
        return true;
    }
    mouseDown(mx:number, my:number):boolean {
        return true;
    }
    mouseMove(mx:number, my:number):boolean {
        return true;
    }
    select():void {
        this.isSelected = true;
    }
    unselect():void {
        this.isSelected = false;
    }

    serialize():any {
        let data = {};
        data["type"] = CInteractable.ClassName;
        return data;
    }
}

// Represents the visual components of the selection box
export class CDrawableSelectionBox extends CDrawable {
    static readonly ClassName:string = "CDrawableSelectionBox";

    private selectedEntity:Entity;

    public constructor(_entity:Entity, _material:Material = null, _selected_entity:Entity) {
        super(_entity, _material);
        this.selectedEntity = _selected_entity;
        this.material = new Material(true, true, "#ffffff",
            "#000000", 1.0);
    }

    draw(context:CanvasRenderingContext2D, material_override:Material = null):void {
        let aabb = this.selectedEntity.drawable.getAABB();
        const corners = [[-1, -1], [1, -1], [1, 1], [-1, 1]];
        context.save();
        context.beginPath();
        context.strokeStyle = "#0f0f0f";
        context.lineWidth = 2.0;
        context.strokeRect(aabb.minX + 2, aabb.minY + 2, aabb.width - 4, aabb.height - 4);
        context.beginPath();
        context.strokeStyle = "#ffffff";
        context.lineWidth = 3.0;
        context.strokeRect(aabb.minX + 4, aabb.minY + 4, aabb.width - 8, aabb.height - 8);

        context.strokeStyle = this.material.strokeColor;
        context.lineWidth = 2.0;
        context.fillStyle = this.material.fillColor;
        for(let c of corners) {
            context.beginPath();
            context.arc(aabb.center[0] + (c[0] * aabb.width / 2) + (-c[0] * 4),
                aabb.center[1] + (c[1] * aabb.height / 2) - (c[1] * 4), 4, 0,
                2 * Math.PI);
            context.fill();
            context.stroke();
        }

        context.beginPath();
        context.strokeStyle = "#ffffff";
        context.lineWidth = 1.0;
        context.setLineDash([2, 2]);
        context.strokeRect(aabb.minX, aabb.minY, aabb.width, aabb.height);
        context.beginPath();
        context.moveTo(aabb.center[0], aabb.minY);
        context.lineTo(aabb.center[0], aabb.minY - 20);
        context.setLineDash([2, 2]);
        context.stroke();
        context.setLineDash([]);
        context.beginPath();
        context.arc(aabb.center[0], aabb.minY - 20, 7, 0, 2 * Math.PI);
        context.fill();
        context.stroke();
        context.restore();
    }
    
    serialize():any {
        assert(false, "CDrawableSelectionBox should never be serialized.");
        return null;
    }
}

// Fixed with workaround: issue where first call to beginDrag drawable AABB is incorrect
// and always lead to scale lower-right corner state. Subsequent calls
// work perfectly (AABB is correct).
export class CUISelectionBox implements IUIComponent {
    static readonly ClassName:string = "CUISelectionBox";

    protected transformBox:TransformBox;
    protected drawable:IDrawable;
    protected m_mouseOver:boolean = false;
    protected beingDragged:boolean = false;
    protected lastDragPosition:vec2;
    protected transformMode:number;
    protected readonly rotationControlOffsetY:number = -20;
    protected readonly rotationControlRadius:number = 7;

    public constructor(_entity:TransformBox, _drawable:IDrawable) {
        assert(_drawable != null && _entity != null);
        this.transformBox = _entity;
        this.drawable = _drawable;
        this.m_mouseOver = false;
        this.beingDragged = false;
        this.lastDragPosition = new vec2();
        this.transformMode = 0;
    }
    
    isPointInBox(mx:number, my:number):boolean {
        let aabb:AABB = this.boundingBox;
        let result = false;
        if(aabb.isPointInside(mx, my)) {
            result = true;
        }
        return result;
    }

    isPointInRotation(mx:number, my:number):boolean {
        let aabb:AABB = this.boundingBox;
        let center = new vec2(aabb.center[0], aabb.minY + this.rotationControlOffsetY);
        let result = false;
        if(v2distance2(center, new vec2(mx, my)) < Math.pow(this.rotationControlRadius, 2)) {
            result = true;
        }
        return result;
    }

    determineMode(x:number, y:number):number {
        //let aabb:AABB = this.boundingBox;
        let aabb:AABB = StaticComponent.selectionBoxUI.boundingBox;
        let mode:number = 0;
        if(aabb.isPointInside(x, y)) {
            let horizontal = 0; // -1 left, 0 middle, +1 right
            let vertical = 0; // -1 top, 0 middle, +1 bottom
            if(x < aabb.minX + TRANSFORM_BOX_SCALE_BORDER) {
                // left
                horizontal = -1;
            } else if(x > aabb.maxX - TRANSFORM_BOX_SCALE_BORDER) {
                // right
                horizontal = 1;
            }
            if(y < aabb.minY + TRANSFORM_BOX_SCALE_BORDER) {
                // top
                vertical = -1;
                if(horizontal == -1) {
                    mode = 8;
                } else if(horizontal == 1) {
                    mode = 2;
                } else {
                    mode = 1;
                }
            } else if(y > aabb.maxY - TRANSFORM_BOX_SCALE_BORDER) {
                // bottom
                vertical = 1;
                if(horizontal == -1) {
                    mode = 6;
                } else if(horizontal == 1) {
                    mode = 4;
                } else {
                    mode = 5;
                }
            } else {
                // middle
                if(horizontal == -1) {
                    mode = 7;
                } else if(horizontal == 1) {
                    mode = 3;
                } else {
                    // else it'll be move direction = 0
                }
            }
        } else if(this.isPointInRotation(x, y)) {
            mode = 9;
        }
        return mode;
    }

    applyTransform(delta:vec2):void {
        switch(this.transformMode) {
        case 0: // Move
            this.transformBox.moveBy(delta);
            break;
        case 1: // N
            this.transformBox.scaleBy(0.0, -delta.y);
            break;
        case 2: // NE
            this.transformBox.scaleBy(delta.x, -delta.y);
            break;
        case 3: // E
            this.transformBox.scaleBy(delta.x, 0.0);
            break;
        case 4: // SE
            this.transformBox.scaleBy(delta.x, delta.y);
            break;
        case 5: // S
            this.transformBox.scaleBy(0.0, delta.y);
            break;
        case 6: // SW
            this.transformBox.scaleBy(-delta.x, delta.y);
            break;
        case 7: // W
            this.transformBox.scaleBy(-delta.x, 0.0);
            break;
        case 8: // NW
            this.transformBox.scaleBy(-delta.x, -delta.y);
            break;
        case 9: // Rotation
            this.transformBox.rotateBy(delta);
            break;
        }
    }

    beginDrag(x:number, y:number):void {
        if(!this.beingDragged) {
            this.beingDragged = true;
            this.lastDragPosition.set(x, y);
            //this.transformMode = this.determineMode(x, y);
        }
    }

    updateDrag(x:number, y:number):void {
        if(this.beingDragged) {
            this.applyTransform(v2sub(new vec2(x, y), 
                                      this.lastDragPosition));
            //this.transformBox.moveBy(v2sub(new vec2(x, y), 
            //        this.lastDragPosition));
            this.lastDragPosition.set(x, y);
        }
    }

    endDrag(x:number, y:number):void {
        if(this.beingDragged) {
            this.beingDragged = false;
            this.lastDragPosition.set(x, y);
            StaticComponent.ResetMouseCursor();
        }
    }

    mouseDown(mx:number, my:number):boolean {
        if(!this.beingDragged) {
            this.beginDrag(mx, my);
            StaticComponent.SetMouseCursorTransform(this.transformMode);
        }
        return true;
    }

    mouseUp(mx:number, my:number):boolean {
        if(this.beingDragged) {
            this.endDrag(mx, my);
        }
        StaticComponent.ResetMouseCursor();
        return true;
    }

    mouseMove(mx:number, my:number):boolean {
        if(this.beingDragged) {
            this.updateDrag(mx, my);
        } else {
            this.transformMode = this.determineMode(mx, my);
            StaticComponent.SetMouseCursorTransform(this.transformMode);
        }
        return true;
    }

    mouseExit():boolean {
        StaticComponent.ResetMouseCursor();
        return true;
    }

    mouseEnter():boolean {
        //StaticComponent.SetMouseCursorToCross();
        return true;
    }

    // This is update by App, will call MouseExit and MouseEnter appropriately
    set mouseOver(value:boolean) {
        if(this.m_mouseOver != value) {
            if(value) {
                this.mouseEnter(); 
            } else {
                this.mouseExit();
            }
        }
        this.m_mouseOver = value;
    }

    get boundingBox():AABB {
        return this.drawable.getAABB();
    }

    get isBeingDragged():boolean {
        return this.beingDragged;
    }

    serialize():any {
        assert(false, "CUISelectionBox should never be serialized.");
        return null;
    }
}

// Provides access to shared data/functionality
export class StaticComponent {
    public static canvas:HTMLCanvasElement = null;
    public static collisionCanvas:HTMLCanvasElement = null;
    public static selectionBoxUI:CUISelectionBox = null;

    public static Initialize(_canvas:HTMLCanvasElement, _collision_canvas:HTMLCanvasElement) {
        this.canvas = _canvas;
        this.collisionCanvas = _collision_canvas;
        this.selectionBoxUI = null;
    }

    public static ResetMouseCursor():void {
        assert(this.canvas);
        this.canvas.style.cursor = "auto";
    }
    public static SetMouseCursorToCross():void {
        assert(this.canvas);
        this.canvas.style.cursor = 'move';
    }
    // Direction/Mode:
    //   8  1  2
    //   7  *  3
    //   6  5  4          
    public static SetMouseCursorTransform(mode:number):void {
        assert(mode <= 9, "SetMouseCursorTransform() unknown mode: " + mode);
        const cursors = ['move', 'n-resize', 'ne-resize', 
            'e-resize', 'se-resize', 's-resize', 'sw-resize',
            'w-resize', 'nw-resize', 'col-resize'];
        this.canvas.style.cursor = cursors[mode];
    }

    // Parses data provided from serialization and creates correct 
    // components.
    public static UnserializeComponent(owner:Entity, data:any):IComponent {
        let comp:IComponent = null;
        if(data.type == CDrawableVertices.ClassName) {
            let dv = new Vertices(null, Transform.unserialize(data.verticesTransform));
            for(let v of data.vertices) {
                dv.add(v.x, v.y);
            }
            comp = new CDrawableVertices(owner, <Material>data["material"], dv);
        } else if(data.type == CInteractable.ClassName) {
            comp = new CInteractable(owner.id, owner.drawable);
        } else {
            console.log("StaticComponent::UnserializeComponent() given incorrect component or unserialiable component: " + data.type);
        }
        if(comp) {
            console.log("    UnserializeComponent -> " + data.type);
        }
        return comp;
    }

    public static calcRectVerts(w:number, h:number):Vertices {
        let rc = new Vertices();
        rc.add(- w / 2, - h / 2);
        rc.add(+ w / 2, - h / 2);
        rc.add(+ w / 2, + h / 2);
        rc.add(- w / 2, + h / 2);
        return rc;
    }

    public static calcCircleVerts(radius:number):Vertices {
        let ci = new Vertices();
        for(let r = 0.0; r < 2*Math.PI; r+=2*Math.PI/CIRCLE_SEGMENTS) {
            ci.add(Math.cos(r) * radius, Math.sin(r) * radius);
        }
        return ci;
    }

    public static calcTriangleVerts(base_width:number, height:number):Vertices {
        let tri = new Vertices();
        let x = - base_width / 2;
        let y = - height / 2;
        tri.add(x + base_width / 2, y);
        tri.add(x + base_width, y + height);
        tri.add(x, y + height);
        return tri;
    }

    public static calcStarVerts(radius:number):Vertices {
        // Parametric drawing allows variations in radius and a star of
        // any number.
        let st = new Vertices();
        let inner_radius = 0.5 * radius;
        let points = 5.0; // pointy points count
        let angle = Math.PI * 2 / (points * 2);
        for(let i = (points * 2) + 1; i > 0; --i) {
            let r = (i % 2) == 1? radius : inner_radius; 
            let omega = angle * i;
            let tx = (r * Math.sin(omega));
            let ty = (r * Math.cos(omega));
            st.add(tx, ty + 7); // star slightly offset upwards
        }
        return st;
    }
}

// --------------------- Entity system
// Entity object class: represents an object in app space
// This is the worker horse in the system.
// OPTIMIZE: group components by implementation types for efficient iteration
export class Entity implements IEntity {
    static readonly ClassName:string = "Entity";
    // Non-Serializable 
    private static nextId:number = 1; // auto-incremented id
    interactable:IInteractable; // Last interactable component stored for convenience
    drawable:IDrawable;         // Last drawable component stored for convenience

    // Serializable
    transform:Transform;
    
    protected components:Array<IComponent>;
    protected m_isSaveData:boolean;// if true, entity will be serialized upon saving
    protected m_layer:number;     // Draw layer, smaller layers will be drawn first

    private entityId:number = 1; // auto-generated id

    public constructor(_layer:number) {
        this.components = new Array<IComponent>();
        this.transform = new Transform(0, 0);
        this.interactable = null;
        this.entityId = Entity.nextId;
        this.m_layer = _layer;
        Entity.nextId += 1;
        this.m_isSaveData = false;
    }

    public static unserialize(data:any):Entity {
        let entity = new Entity(data.m_layer);
        entity.transform = Transform.unserialize(data.m_transform);
        let comps = data.m_components;
        Entity.unserializeComponents(entity, comps);
        return entity;
    }

    // Unserializes all components in given array and adds them to entity
    protected static unserializeComponents(entity:Entity, comps:any) {
        for(let c of comps) {
            // This will also set the correct interactable and drawable data for Entity
           let component = StaticComponent.UnserializeComponent(entity, c);
           if(component) {
               entity.addComponent(component);
           }
        }
    }

    serialize():any {
        let obj = {};
        obj["type"]         = Entity.ClassName;
        obj["entityId"]     = this.entityId;
        obj["m_layer"]      = this.m_layer;
        let comps = [];
        for(let c of this.components) {
            comps.push(c.serialize());
        }
        obj["m_components"] = comps;
        obj["m_transform"]  = this.transform;
        // interactable and drawable are updated automatically based on components
        return obj;
    }

    addComponent(comp:IComponent):void {
        this.components.push(comp);
        if(isInteractable(comp)) {
            this.interactable = comp;
        }
        if(isDrawable(comp)) {
            this.drawable = comp;
        }
    }

    update(time_delta:number):void {
        for(let c of this.components) {
            if(isUpdatable(c)) {
                c.update(time_delta);
            }
        }
    }

    // Draws all drawable components
    draw(context:CanvasRenderingContext2D):void {
        for(let c of this.components) {
            if(isDrawable(c)) {
                c.draw(context, null);
            }
        }
    }

    // Draws all IInteractable components
    drawInteractables(context:CanvasRenderingContext2D, collision_context:CanvasRenderingContext2D):void {
        for(let c of this.components) {
            if(isInteractable(c)) {
                c.drawInteraction(context, collision_context);
            }
        }
    }

    // Events return boolean indicating whether they are consumed or not
    // consumed events don't run for other overlapping objects.
    mouseDown(mx:number, my:number):boolean {
        let consumed = false;
        if(this.interactable != null) {
            consumed = this.interactable.mouseDown(mx, my);
        }
        return consumed;
    }
    mouseUp(mx:number, my:number):boolean {
        let consumed = false;
        if(this.interactable != null) {
            consumed = this.interactable.mouseUp(mx, my);
        }
        return consumed;
    }
    mouseMove(mx:number, my:number):boolean {
        let consumed = false;
        return consumed;
    }
    mouseEnter():boolean {
        let consumed = false;
        if(this.interactable != null) {
            consumed = this.interactable.mouseEnter();
        }
        return consumed;
    }
    mouseExit():boolean {
        let consumed = false;
        if(this.interactable != null) {
            consumed = this.interactable.mouseExit();
        }
        return consumed;
    }
    select():boolean {
        if(this.interactable != null) {
            this.interactable.select();
            return true;
        }
        return false;
    }

    unselect():boolean {
        if(this.interactable != null) {
            this.interactable.unselect();
            return true;
        }
        return false;
    }

    notifyRemoval():void {
        // Entity is about to be removed
    }

    mirrorH():void {
        this.transform.scalex = -this.transform.scalex;
    }

    mirrorV():void {
        this.transform.scaley = -this.transform.scaley;
    }



    get id():number {
        return this.entityId;
    }

    get layer():number {
        return this.m_layer;
    }

    set layer(value:number) {
        this.m_layer = value;
        if(this.m_layer < 0) {
            this.m_layer = 1;
        }
    }

    get isSaveData():boolean {
        return this.m_isSaveData;
    }
}

// --------------------- App high-level entities
export class Shape extends Entity {
    static readonly ClassName:string = "Shape";

    // Serializable
    material:Material;

    public constructor(_material:Material) {
        super(SHAPES_LAYER);
        this.material = _material;
        this.m_isSaveData = true;
    }

    public static unserialize(data:any):Entity {
        let entity = new Shape(<Material>data.material);
        entity.transform = Transform.unserialize(data.m_transform);
        entity.m_layer = data.m_layer;
        let comps = data.m_components;
        Entity.unserializeComponents(entity, comps);
        return entity;
    }

    addRectComponent(cx:number, cy:number, w:number, h:number):void {
        this.transform.x = cx;
        this.transform.y = cy;
        let d = new CDrawableVertices(this, this.material, StaticComponent.calcRectVerts(w, h));
        this.addComponent(d);
        this.addComponent(new CInteractable(this.id, d));
    }

    addCircleComponent(cx:number, cy:number, radius:number):void {
        this.transform.x = cx;
        this.transform.y = cy;
        let d = new CDrawableVertices(this, this.material, StaticComponent.calcCircleVerts(radius));
        this.addComponent(d);
        this.addComponent(new CInteractable(this.id, d));
    }

    addTriangleComponent(cx:number, cy:number, base_width:number, height:number):void {
        this.transform.x = cx;
        this.transform.y = cy;
        let d = new CDrawableVertices(this, this.material, StaticComponent.calcTriangleVerts(base_width, height));
        this.addComponent(d);
        this.addComponent(new CInteractable(this.id, d));
    }

    addStarComponent(cx:number, cy:number, _radius:number):void {
        this.transform.x = cx;
        this.transform.y = cy;
        
        let d = new CDrawableVertices(this, this.material, StaticComponent.calcStarVerts(_radius));
        this.addComponent(d);
        this.addComponent(new CInteractable(this.id, d));
    }

    serialize():any {
        let data = super.serialize();
        data.type = Shape.ClassName; // overrides parent type name
        data.material = this.material;
        return data;
    }
}

export class TransformBox extends Entity {
    static readonly ClassName:string = "TransformBox";
    private material:Material;
    private selectedEntity:Entity;
    private uiSelectionBox:CUISelectionBox;

    public constructor(selected_entity:Entity) {
        super(TRANSFORM_BOX_LAYER);
        this.selectedEntity = selected_entity;
        this.transform.x = selected_entity.transform.x;
        this.transform.y = selected_entity.transform.y;
        this.material = new Material(false, true, "", "#efefff", 3.0);
        this.addComponents(this.transform.x, this.transform.y);
        StaticComponent.selectionBoxUI = this.uiSelectionBox;
        this.m_isSaveData = false; // should not be serialized
    }

    addComponents(cx:number, cy:number):void {
        this.transform.x = cx;
        this.transform.y = cy;
        let aabb = this.selectedEntity.drawable.getAABB();
        this.uiSelectionBox = new CUISelectionBox(this, this.selectedEntity.drawable);
        this.addComponent(this.uiSelectionBox);
        let scalers = new CDrawableSelectionBox(this, null, this.selectedEntity);
        this.addComponent(scalers);
        this.drawable = this.selectedEntity.drawable;
    }

    notifyRemoval():void {
        if(StaticComponent.selectionBoxUI == this.uiSelectionBox) {
            StaticComponent.ResetMouseCursor();
            StaticComponent.selectionBoxUI = null;
        }
    }

    moveBy(amount:vec2):void {
        this.transform.x += amount.x;
        this.transform.y += amount.y;
        this.selectedEntity.transform.x += amount.x;
        this.selectedEntity.transform.y += amount.y;
    }

    rotateBy(amount:vec2):void {
        let angle = amount.x * Math.PI / 180.0; // delta x to radians
        this.transform.rotation = (angle) % (2 * Math.PI);
        this.selectedEntity.transform.rotation = angle % (2 * Math.PI);
    }

    scaleBy(sx:number, sy:number):void {
        let aabb = this.uiSelectionBox.boundingBox;
        sx = sx * 4  / aabb.width;
        sy = sy * 4 / aabb.height;
        this.transform.scalex = this.transform.scalex; //Math.max(this.transform.scalex + sx, 0.1);
        this.transform.scaley = this.transform.scaley; //Math.max(this.transform.scaley + sy, 0.1);
        this.selectedEntity.transform.scalex = Math.max(this.selectedEntity.transform.scalex + sx, 0.1);
        this.selectedEntity.transform.scaley = Math.max(this.selectedEntity.transform.scaley + sy, 0.1);
    }
}

//--------------------------------------------- EntitySystem
// Used to contain scene entities
export class EntitySystem {
    private entitySort:Array<number>;
    private entities:Array<Entity>;
    private resortNeeded:boolean;

    public constructor() {
        this.entitySort = new Array<number>();
        this.entities = new Array<Entity>();
        this.resortNeeded = false;
    }

    public addEntity(entity:Entity):void {
        // Actual entity in sequential indices
        this.entities.push(entity);
        // Entity place stores: sort place -> index of entity in list
        this.entitySort.push(this.entities.length - 1);
        this.resortEntities();
    }

    public removeEntity(entity:Entity):void {
        var idx = this.entities.indexOf(entity);
        if(idx > -1) {
            this.entities[idx].notifyRemoval();
            var si = this.entitySort.indexOf(idx);
            this.entitySort.splice(si, 1);
            this.entities.splice(idx, 1);
            // Rebuild entity sort
            for(var i = 0; i < this.entities.length; ++i) {
                this.entitySort[i] = i;
            }
            this.executeSortEntities();
        }
    }

    public removeAllEntities():void {
        this.entitySort = new Array<number>();
        this.entities = new Array<Entity>();
    }

    public resortEntities():void {
        this.resortNeeded = true;
    }
    
    private executeSortEntities():void {
        // Sort by layer
        this.entitySort.sort((a, b) => {
            return this.entities[a].layer - this.entities[b].layer;
        });
    }

    public get(entity_id):Entity {
        for(let i = 0; i < this.entities.length; ++i) {
            if(this.entities[i].id == entity_id) {
                return this.entities[i];
            }
        }
        return null;
    }

    public update(time_delta:number):void {
        if(this.resortNeeded) {
            this.resortNeeded = false;
            this.executeSortEntities();
        }
        for(let e of this.entities) {
            e.update(time_delta);
        }
    }

    public draw(context:CanvasRenderingContext2D, 
                collision_context:CanvasRenderingContext2D):void {
        for(let i = 0; i < this.entitySort.length; ++i) {
            this.entities[this.entitySort[i]].draw(context);
            this.entities[this.entitySort[i]].drawInteractables(context, collision_context);
        }
    }

    public get entitiesArray():Array<Entity> {
        return this.entities;
    }
}
