# Shape-Verse (ShapeComposer internally)

Project uses the following solutions:

- Typescript
- Canvas API
- Angular 2
- Karma/Jasmine

The development strategy was to implement a simple ECS-system to allow creation of the required features as components and entities. Then create the app on top of that.

Additionally using Angular 2 form features to provide a simple practical html-based UI.

Typescript had one issue that prevents a successful build, its internal interface for canvas is missing the function: fill(Path2D).

I added that function to all module files related to canvas in node-modules so typescript can find it.

The modified files are in the folder: 'patch_for_canvas_fill' and they should placed to overwrite files in node_modules folder if the issue appeared during testing.

## Running App

Just open: 'release/index.html' in chrome.

## Running unit tests

Implemented math tests as required, run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

